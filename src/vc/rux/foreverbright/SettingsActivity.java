package vc.rux.foreverbright;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends Activity {
    private CheckBox mKeepScreenOn;
    private CheckBox mStartApp;
    private ApplicationsAdapter mApplicationsAdapter;
    private Config mConfig;
    private PackageManager mPM;
    private App mApp;
    private Button mChooseApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mKeepScreenOn = (CheckBox) findViewById(R.id.enableAutostart);
        mStartApp = (CheckBox) findViewById(R.id.startAppOnAutorun);
        mChooseApp = (Button) findViewById(R.id.chooseApp);

        mApplicationsAdapter = new ApplicationsAdapter();
        mConfig = new Config(this);

        mPM = getPackageManager();
        mApp = (App)getApplicationContext();

        mApp.updateLock();

        mKeepScreenOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mConfig.setAutostart(b);
                mApp.updateLock();
            }
        });

        mStartApp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mConfig.setStartApp(b);
            }
        });


        mChooseApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                builder.setTitle("Select");
                builder.setAdapter(mApplicationsAdapter,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,  int item) {
                                ResolveInfo ri =  mApplicationsAdapter.getItem(item);
                                mConfig.setApp(ri.resolvePackageName);
                                dialog.dismiss();
                                mChooseApp.setText(ri.loadLabel(mPM));
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        if (mConfig.getApp() != null && mConfig.getApp().length() > 0) {
            mChooseApp.setText(mConfig.getApp());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mKeepScreenOn.setChecked(mConfig.isAutostart());
        mStartApp.setChecked(mConfig.isStartApp());

        mApplicationsAdapter.reload();

        String selectedApp = mConfig.getApp();
        for(int i = 0; i < mApplicationsAdapter.getCount(); i++) {
            if (selectedApp.equals(mApplicationsAdapter.getItem(i).resolvePackageName)) {
                mChooseApp.setText(mApplicationsAdapter.getItem(i).loadLabel(mPM));
                break;
            }
        }
    }

    private List<ResolveInfo> enumerateApplications() {
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> pkgAppsList = getPackageManager().queryIntentActivities(mainIntent, 0);
        return pkgAppsList;
    }

    class ApplicationsAdapter extends BaseAdapter {
        List<ResolveInfo> list = new ArrayList<ResolveInfo>();

        public ApplicationsAdapter() {
        }

        public void reload() {
            setData(enumerateApplications());
        }

        public void setData(List<ResolveInfo> infoList) {
            list.clear();
            list = infoList;
            notifyDataSetInvalidated();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public ResolveInfo getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            return generateView(position, view, parent);
        }

        public View generateView(int position, View view, ViewGroup parent) {
            ResolveInfo item = getItem(position);
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.app_row, parent, false);
            }
            TextView label = (TextView) view.findViewById(R.id.appRowLabel);
            ImageView icon = (ImageView) view.findViewById(R.id.appRowIcon);

            label.setText(item.loadLabel(mPM));
            icon.setImageDrawable(item.loadIcon(mPM));

            return view;
        }


        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return generateView(position, convertView, parent);
        }
    }

}

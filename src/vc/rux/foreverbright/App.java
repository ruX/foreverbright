package vc.rux.foreverbright;

import android.app.Application;
import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;


/**
 * @author ruX[Ruslan Zaharov]
 */
public class App extends Application {
    private PowerManager.WakeLock mWakeLock;
    private PowerManager mPM;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("App", "Application started!");

        mPM = (PowerManager) getSystemService(Context.POWER_SERVICE);

        mWakeLock = mPM.newWakeLock(PowerManager.FULL_WAKE_LOCK, "okTag");

        updateLock();
    }

    public void updateLock() {
        boolean isAutostart = new Config(this).isAutostart();
        Log.d("App", "updateLock called. Enabling: " + isAutostart);
        if (isAutostart) {
            if (!mWakeLock.isHeld()) {
                mWakeLock.acquire();
                mPM.userActivity(SystemClock.uptimeMillis(), false);
            }
        } else {
            mWakeLock.release();
        }
    }
}

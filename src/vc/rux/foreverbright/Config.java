package vc.rux.foreverbright;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Configuration wrapper
 * @author ruX[Ruslan Zaharov]
 */

public class Config {
    private Context context;

    private static final String PREFS = "cfg";

    public Config(Context context) {
        this.context = context;
    }

    public void setAutostart(boolean autostart) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean("autostart", autostart);
        editor.commit();
    }

    public boolean isAutostart() {
        return getSharedPreferences().getBoolean("autostart", false);
    }


    public void setStartApp(boolean startapp) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean("startapp", startapp);
        editor.commit();
    }

    public boolean isStartApp() {
        return getSharedPreferences().getBoolean("startapp", false);
    }


    public void setApp(String app) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("app", app);
        editor.commit();
    }

    public String getApp() {
        return getSharedPreferences().getString("app", "");
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PREFS, 0);
    }
}

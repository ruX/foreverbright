package vc.rux.foreverbright;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * @author ruX[Ruslan Zaharov]
 */
public class BootUpReceiver extends BroadcastReceiver {
    protected final String TAG = getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Config cfg = new Config(context);
        if (!cfg.isAutostart()) {
            Log.i(TAG, "Auto start disabled.");
        }
        Log.i(TAG, "Auto start enabled.");
        ((App)context.getApplicationContext()).updateLock();

        String appToStart = cfg.getApp();
        if (appToStart != null || appToStart.length() != 0) {
            Log.i(TAG, "Starting app " + appToStart);
            Intent i = context.getPackageManager().getLaunchIntentForPackage(appToStart);
            context.startActivity(i);
        }

//        //Intent i = new Intent(context, MainActivity.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        context.startActivity(i);
    }
}
